export default {
  title: "Magazyny",
  name: "magazine",
  type: "document",
  fields: [
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: (doc) => `${doc.title}-${doc.issueNumber}`,
        slugify: (input) => {
          const from =
            "ąćęłńóśźżãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
          const to =
            "acelnosczzaaaaaeeeeeiiiiooooouuuunc------";

          from.split("").forEach((fromItem, index) => {
            console.log(fromItem);
            input = input
              .toLowerCase()
              .replace(
                new RegExp(from.charAt(index), "g"),
                to.charAt(index)
              );
          });

          return input
            .trim()
            .replace(/\s+/g, "-")
            .replace(/&/g, "-y-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .slice(0, 200);
        },
      },
    },
    {
      title: "Tytuł",
      name: "title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Numer wydania",
      name: "issueNumber",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "datePublished",
      title: "Data wydania",
      type: "date",
    },
    {
      name: "period",
      title: "Częstotliwość wydania",
      type: "string",
      options: {
        list: [
          { title: "Tygodnik", value: "weekly" },
          { title: "Miesięcznik", value: "monthly" },
          {
            title: "Rocznik",
            value: "annually",
          },
          { title: "Nieregularny", value: "irregular" },
        ],
      },
      validation: (Rule) => Rule.required(),
    },
    {
      name: "articles",
      title: "Artykuły",
      type: "array",
      of: [{ type: "string" }],
    },
    {
      name: "images",
      title: "Obrazy",
      type: "array",
      of: [{ type: "customImage" }],
    },
  ],
  preview: {
    select: {
      title: "title",
      issueNumber: "issueNumber",
      image: "images.0.image",
    },
    prepare: ({ title, issueNumber, image }) => {
      return {
        title: title,
        subtitle: issueNumber,
        description: null,
        media: image,
      };
    },
  },
};
