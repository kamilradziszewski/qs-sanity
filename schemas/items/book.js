export default {
  title: "Książki",
  name: "book",
  type: "document",
  fields: [
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: (doc) => `${doc.author}-${doc.title}`,
        slugify: (input) => {
          const from =
            "ąćęłńóśźżãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
          const to =
            "acelnosczzaaaaaeeeeeiiiiooooouuuunc------";

          from.split("").forEach((fromItem, index) => {
            console.log(fromItem);
            input = input
              .toLowerCase()
              .replace(
                new RegExp(from.charAt(index), "g"),
                to.charAt(index)
              );
          });

          return input
            .trim()
            .replace(/\s+/g, "-")
            .replace(/&/g, "-y-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .slice(0, 200);
        },
      },
    },
    {
      title: "Tytuł",
      name: "title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Autor",
      name: "author",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "description",
      title: "Opis",
      type: "array",
      of: [{ type: "block" }],
    },
    {
      name: "datePublished",
      title: "Data publikacji",
      type: "date",
    },
    {
      name: "isbn",
      title: "ISBN",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "numOfPages",
      title: "Ilość stron",
      type: "number",
    },
    {
      name: "images",
      title: "Obrazy",
      type: "array",
      of: [{ type: "customImage" }],
    },
  ],
  preview: {
    select: {
      title: "title",
      author: "author",
      image: "images.0.image",
    },
    prepare: ({ title, author, image }) => {
      return {
        title: `${title}`,
        subtitle: `${author}`,
        description: null,
        media: image,
      };
    },
  },
};
