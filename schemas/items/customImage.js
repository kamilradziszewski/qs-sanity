export default {
  title: "Obraz",
  name: "customImage",
  type: "object",
  fields: [
    { name: "image", title: "Image", type: "image" },
    {
      name: "alt",
      type: "string",
      title: "Alt Text",
      options: {
        isHighlighted: true,
      },
      validation: (Rule) => Rule.required(),
    },
  ],
};
