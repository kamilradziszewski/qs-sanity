export default {
  title: "Ulotki",
  name: "flyer",
  type: "document",
  fields: [
    {
      title: "Slug",
      name: "slug",
      type: "slug",
      validation: (Rule) => Rule.required(),
      options: {
        source: (doc) => `${doc.title}-${doc.organization}`,
        slugify: (input) => {
          const from =
            "ąćęłńóśźżãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
          const to =
            "acelnosczzaaaaaeeeeeiiiiooooouuuunc------";

          from.split("").forEach((fromItem, index) => {
            console.log(fromItem);
            input = input
              .toLowerCase()
              .replace(
                new RegExp(from.charAt(index), "g"),
                to.charAt(index)
              );
          });

          return input
            .trim()
            .replace(/\s+/g, "-")
            .replace(/&/g, "-y-")
            .replace(/[^\w\-]+/g, "")
            .replace(/\-\-+/g, "-")
            .slice(0, 200);
        },
      },
    },
    {
      title: "Tytuł",
      name: "title",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      title: "Organizator",
      name: "organization",
      type: "string",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "date",
      title: "Data",
      type: "date",
    },
    {
      name: "location",
      title: "Lokalizacja",
      type: "string",
    },
    {
      name: "description",
      title: "Opis",
      type: "array",
      of: [{ type: "block" }],
    },
    {
      name: "images",
      title: "Obrazy",
      type: "array",
      of: [{ type: "customImage" }],
    },
  ],
  preview: {
    select: {
      title: "title",
      issueNumber: "issueNumber",
      image: "images.0.image",
    },
    prepare: ({ title, issueNumber, image }) => {
      return {
        title: title,
        subtitle: issueNumber,
        description: null,
        media: image,
      };
    },
  },
};
