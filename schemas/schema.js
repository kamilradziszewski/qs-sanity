import createSchema from "part:@sanity/base/schema-creator";
import schemaTypes from "all:part:@sanity/base/schema-type";

import book from "./items/book";
import magazine from "./items/magazine";
import flyer from "./items/flyer";
import customImage from "./items/customImage";

export default createSchema({
  name: "default",
  types: schemaTypes.concat([
    book,
    magazine,
    flyer,
    customImage,
  ]),
});
